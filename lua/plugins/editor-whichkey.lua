return {
  -- 键位 map
  "folke/which-key.nvim",
  opts = {
    plugins = { spelling = true },
  },
  config = function(_, opts)
    local wk = require('which-key')
    wk.setup(opts)
    wk.add({
      { "<leader>f", group = "File" }, 
      { mode = { "n", "v" }, "<leader>g", group = "Git" }, -- git signs
      { "<leader>u", group = "Ui" }, 
      { "<leader>ut", group = "Treesitter" }, 
      { mode = { "n", "v" }, "<leader>t", group = "Translate" }, -- Translate
      { mode = { "n", "v" }, "<leader>tc", group = "Into Chinese" },
      { mode = { "n", "v" }, "<leader>k", hidden = true },
      { mode = { "n", "v" }, "<leader>K", hidden = true },
      -- { "<leader>ff", "<cmd>Telescope find_files<cr>", desc = "Find File", mode = "n" },
      -- { "<leader>fb", function() print("hello") end, desc = "Foobar" },
      -- { "<leader>fn", desc = "New File" },
      -- { "<leader>f1", hidden = true }, -- hide this keymap
      -- { "<leader>b", group = "buffers", expand = function()
        --   return require("which-key.extras").expand.buf()
        -- end
      -- },
      {
        mode = { "n", "v" }, -- NORMAL and VISUAL mode
        { "<leader>q", "<cmd>q<cr>", desc = "Quit" }, -- no need to specify mode since it's inherited
        { "<leader>w", proxy = "<c-w>", group = "Windows" }, -- proxy to window mappings

      }
    })
  end,
}
