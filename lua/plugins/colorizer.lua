return {
  {
    -- 没有外部依赖,用lua写的着色器
    "NvChad/nvim-colorizer.lua",
    config = function()
      require("colorizer").setup({
        user_default_options = {
          RRGGBBAA = true,
          rgb_fn = true,
          hls_fn = true,
        },
      })
    end,
  },
  {
    -- 文本调整颜色
    "ziontee113/color-picker.nvim",
    config = function()
      local G = require('G')
      require("color-picker").setup({
        ["icons"] = { "*", "|" },
      })
      G.map({
        { "n", "<C-c>", "<cmd>PickColor<cr>",       { noremap = true, silent = true } },
        { "i", "<C-c>", "<cmd>PickColorInsert<cr>", { noremap = true, silent = true } },
      })
    end,
  },
}
