return {
  -- 回溯撤销的功能
  -- ctrl+r 恢复被 u 键撤销的内容
  -- ctrl+u 查看整个过程
  "mbbill/undotree",
  keys = {
    {
      "<C-u>",
      "<cmd>UndotreeToggle<cr>",
      "n",
      { noremap = true, silent = true, desc = "UndotreeToggle" },
    }
  }
}
