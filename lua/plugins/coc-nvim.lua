return {
  "neoclide/coc.nvim",
  build = "cd && yarn install",
  branch = "release",
  event = "VeryLazy",
  config = function()
    local G = require('G')
    G.g.coc_global_extensions = {
      'coc-marketplace',
      '@yaegassy/coc-volar',
      'coc-tsserver',
      'coc-json',
      'coc-html', 'coc-css',
      'coc-clangd',
      -- 'coc-go',
      'coc-sumneko-lua',
      'coc-vimlsp',
      'coc-sh', 'coc-db',
      'coc-pyright',
      'coc-toml',
      'coc-prettier',
      'coc-snippets', 'coc-pairs', 'coc-word',
      'coc-translator',
      'coc-git',
      'coc-yank',
    }
    G.cmd("command! -nargs=? Fold :call CocAction('fold', <f-args>)")
    G.cmd("hi! link CocPum Pmenu")
    G.cmd("hi! link CocMenuSel PmenuSel")
    G.map({
      { 'n', '<c-y>', ':CocList -A --normal yank<cr>',         { silent = true, desc = "copy_list" } }, -- 历史复制列表
      { 'n', 'gn',   '<Plug>(coc-rename)',                     { silent = true, desc = "coc-rename" } },
      { 'n', 'gd',    '<Plug>(coc-definition)',                { silent = true, desc = "coc-definition" } },
      { 'n', 'gy',    '<Plug>(coc-type-definition)',           { silent = true, desc = "coc-type-definition" } },
      { 'n', 'gi',    '<Plug>(coc-implementation)',            { silent = true, desc = "coc-implementation" } },
      { 'n', 'gr',    '<Plug>(coc-references)',                { silent = true, desc = "coc-references" } },
      { 'n', 'K',     ':call CocAction("doHover")<cr>',        { silent = true, desc = "CocAction" } },
      { 'n', 'mm',    "<Plug>(coc-translator-p)",              { silent = true } },
      { 'v', 'mm',    "<Plug>(coc-translator-pv)",             { silent = true } },
      -- { 'x', 'if',    '<Plug>(coc-funcobj-i)',                 { silent = true, desc = "" } },
      -- { 'o', 'if',    '<Plug>(coc-funcobj-i)',                 { silent = true, desc = "" } },
      -- { 'x', 'af',    '<Plug>(coc-funcobj-a)',                 { silent = true, desc = "" } },
      -- { 'o', 'af',    '<Plug>(coc-funcobj-a)',                 { silent = true, desc = "" } },
      -- { 'x', 'ic',    '<Plug>(coc-classobj-i)',                { silent = true, desc = "" } },
      -- { 'o', 'ic',    '<Plug>(coc-classobj-i)',                { silent = true, desc = "" } },
      -- { 'x', 'ac',    '<Plug>(coc-classobj-a)',                { silent = true, desc = "" } },
      -- { 'o', 'ac',    '<Plug>(coc-classobj-a)',                { silent = true, desc = "" } },
      -- { 'i', '<c-f>', "coc#pum#visible() ? '<c-y>' : '<c-f>'", { silent = true, expr = true } },
      { 'i', '<TAB>',"coc#pum#visible() ? coc#pum#next(1) : col('.') == 1 || getline('.')[col('.') - 2] =~# '\\s' ? \"\\<TAB>\" : coc#refresh()",{ silent = true, noremap = true, expr = true } },
      { 'i', '<s-tab>', "coc#pum#visible() ? coc#pum#prev(1) : \"\\<s-tab>\"",{ silent = true, noremap = true, expr = true } },
      { 'i', '<cr>', "coc#pum#visible() ? coc#pum#confirm() : \"\\<c-g>u\\<cr>\\<c-r>=coc#on_enter()\\<cr>\"",{ silent = true, noremap = true, expr = true } },
      -- { 'i', '<c-y>', "coc#pum#visible() ? coc#pum#confirm() : '<c-y>'",{ silent = true, noremap = true, expr = true } },
      -- { 'n', '<F3>',  ":silent CocRestart<cr>",                  { silent = true, noremap = true } },
      -- { 'n', '<F9>',  ":CocCommand snippets.editSnippets<cr>",   { silent = true, noremap = true } }, -- 编辑snippets
      { 'n', '<F4>', "get(g:, 'coc_enabled', 0) == 1 ? ':CocDisable<cr>' : ':CocEnable<cr>'",{ silent = true, noremap = true, expr = true } }, -- 开关报错提示
      { 'n', 'ge', ":CocList --auto-preview diagnostics<cr>", { silent = true, desc = "Coclist diagnostics" } }, -- 预览报错内容
      { 'n', '(',     "<Plug>(coc-git-prevchunk)",               { silent = true } },
      { 'n', ')',     "<Plug>(coc-git-nextchunk)",               { silent = true } },
      { 'n', 'C',"get(b:, 'coc_git_blame', '') ==# 'Not committed yet' ? \"<Plug>(coc-git-chunkinfo)\" : \"<Plug>(coc-git-commit)\"",{ silent = true, expr = true } },
      { 'n', '\\g',":call coc#config('git.addGBlameToVirtualText',  !get(g:coc_user_config, 'git.addGBlameToVirtualText', 0)) | call nvim_buf_clear_namespace(bufnr(), -1, line('.') - 1, line('.'))<cr>",{ silent = true } },
      { 'x', '=', 'CocHasProvider("formatRange") ? "<Plug>(coc-format-selected)" : "="',{ silent = true, noremap = true, expr = true } },
      { 'n', '=', 'CocHasProvider("formatRange") ? "<Plug>(coc-format-selected)" : "="',{ silent = true, noremap = true, expr = true } },
    })
  end,
}
