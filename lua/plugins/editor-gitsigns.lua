return {
  -- git 状态提示
  "lewis6991/gitsigns.nvim",
  event = { 'BufReadPre', 'BufNewFile'},
  opts = {
    on_attach = function (buffer)
      local gs = package.loaded.gitsigns

      local function map(mode, l, r, desc)
        vim.keymap.set(mode, l, r, { buffer = buffer, desc = desc})
      end

      -- stylua: ignore start
      map("n", "]h", gs.next_hunk, "Next Hunk")
      map("n", "[h", gs.prev_hunk, "Prev Hunk")
      map({"n","v"}, "<leader>gs", "<cmd>Gitsigns stage_hunk<cr>", "Stage Hunk")
      map({"n","v"}, "<leader>gr", "<cmd>Gitsigns reset_hunk<cr>", "Reset Hunk")
      map("n", "<leader>gS", gs.stage_buffer, "Stage Buffer")
      map("n", "<leader>gu", gs.undo_stage_hunk, "Undo Stage Hunk")
      map("n", "<leader>gR", gs.reset_buffer, "Reset Buffer")
      map("n", "<leader>gp", gs.preview_hunk, "Preview Hunk")
      map("n", "<leader>gb", function() gs.blame_line({full = true}) end, "Blame Line")
      map("n", "<leader>gd", gs.diffthis, "Diff This")
      map("n", "<leader>gD", function() gs.diffthis("~") end, "Diff This ~")
      map({"o","x"}, "ih", "<cmd><C-U>Gitsigns select_hunk<CR>", "Gitsigns Select Hunk")
    end
  }
}
