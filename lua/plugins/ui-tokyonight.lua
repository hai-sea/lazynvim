return {
  -- 主题插件
  "folke/tokyonight.nvim",
  lazy = false,
  priority = 1000,
  config = function()
    require("tokyonight").setup({
      style = "moon",
      styles = {
        comments = { italic = false },
        keywords = { italic = true },
        functions = { italic = false },
        variables = { italic = false },
      }
    })
    vim.cmd([[colorscheme tokyonight]])
  end,
}
