return {
  -- fzf
  {
    "junegunn/fzf.vim",
    event = "VeryLazy",
  },
  {
    "junegunn/fzf",
    event = "VeryLazy",
  }
}
