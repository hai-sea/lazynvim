local G = require("G")
return {
  -- 代码上下文显示
  {"nvim-treesitter/nvim-treesitter-context"},
  -- 高亮语法插件
  {"nvim-treesitter/nvim-treesitter",
  -- run = ':TSUpdate',
  -- event = "VeryLazy",
  lazy = false,
  config = function()
    require("nvim-treesitter.configs").setup({
      --  添加不同语言
      ensure_installed = { "vim", "bash", "c", "typescript", "javascript", "lua", "python", "go", "query", "markdown" },
      -- sync_install = true,
      highlight = { enable = true },
      -- indent 模块 用 = 可以代码格式化 全文格式化(ggvG=)
      -- indent = { enable = true },
      -- 不同括号颜色区分
    })
  end,
  },
  G.map({
    { 'n', '<leader>uth', "<cmd>TSBufToggle highlight<cr>",          { noremap = true, silent = true } },
    { 'n', '<leader>utc', "<cmd>TSHighlightCapturesUnderCursor<cr>", { noremap = true, silent = true } },
    { 'n', '<leader>utu', "<cmd>TSNodeUnderCursor<cr>",              { noremap = true, silent = true } },
  }),
}
