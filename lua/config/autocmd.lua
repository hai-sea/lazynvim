-- 取消换行注释
-- 用o换行不注释
vim.api.nvim_create_autocmd({ "BufEnter" }, {
    pattern = { "*" },
    callback = function()
        -- vim.opt.formatoptions = vim.opt.formatoptions - { "c", "r", "o" }
        vim.opt.formatoptions = vim.opt.formatoptions
            - "o" -- O and o, don't continue comments
            + "r" -- But do continue when pressing enter.
    end,
})
-- 识别类型文件添加开头
vim.api.nvim_create_autocmd("BufNewFile", {
  pattern = { "*.sh","*.c" },
  callback = function()
    if vim.fn.expand("%:e") == 'sh' then
        local buffer = vim.api.nvim_get_current_buf()
        vim.api.nvim_buf_set_lines(buffer, 0, 0, false, { "#!/bin/bash" })
        local buf_name = vim.api.nvim_buf_get_name(buffer)
        local file_name = buf_name:match("[^/\\]+$")
        vim.api.nvim_buf_set_lines(buffer, 1, 1, false, { "# @FileName:" .. file_name })
        vim.api.nvim_buf_set_lines(buffer, 2, 2, false, { "# @Author:Hagrid Hwang" })
        local current_time = os.date("%Y-%m-%d")
        vim.api.nvim_buf_set_lines(buffer, 3, 3, false, { "# @since:" .. current_time })
        vim.api.nvim_buf_set_lines(buffer, 4, 4, false, { "# " })
    elseif vim.fn.expand("%:e") == 'c' then
      local buffer = vim.api.nvim_get_current_buf()
      vim.api.nvim_buf_set_lines(buffer, 0, 0, false, { "#include <stdio.h>" })
      vim.api.nvim_buf_set_lines(buffer, 1, 1, false, { "" })
      vim.api.nvim_buf_set_lines(buffer, 2, 2, false, { "int main() {" })
      vim.api.nvim_buf_set_lines(buffer, 4, 4, false, { "  return 0;" })
      vim.api.nvim_buf_set_lines(buffer, 5, 5, false, { "}" })
    end
  end
})

