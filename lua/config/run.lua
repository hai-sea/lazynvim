local api = vim.api

api.nvim_create_autocmd("FileType", {
  pattern = "python",
  callback = function()
    api.nvim_buf_set_keymap(
      0, "n", "<c-i>",
      ":w<cr>:split<cr>:resize -3<cr>:te time /bin/python3 %<cr>i",
      { silent = true, noremap = true }
    )
  end
})

api.nvim_create_autocmd("FileType", {
  pattern = "sh",
  callback = function ()
    api.nvim_buf_set_keymap(
    0, "n", "<c-i>",
    ":w<cr>:split<cr>:resize -3<cr>:te bash %<cr>i",
    { silent = true, noremap = true}
    )
  end
})

api.nvim_create_autocmd("FileType", {
  pattern = "c",
  callback = function ()
    api.nvim_buf_set_keymap(
    0, "n", "<c-i>",
    ":w<cr>:split<cr>:resize -3<cr>:te clang % && ./a.out && rm ./a.out<cr>i",
    { silent = true, noremap = true}
    )
  end
})
