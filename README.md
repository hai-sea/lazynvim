# lazynvim

#### 介绍
个人用的nvim配置
插件管理 : lazy

#### 软件架构
软件架构说明
```bash
.
├── coc-settings.json       -- CoCConfig
├── init.lua                -- 加载lua & lazy插件管理配置
├── lua
│   ├── autocmd.lua         -- 一些自定义功能
│   ├── G.lua               -- 定义全局
│   ├── keymaps.lua         -- 快捷键配置
│   ├── neovide.lua         -- neovide 配置
│   ├── options.lua         -- 基础设置
│   ├── plugins             -- 插件
│   │    ├── ...                -- 插件配置文件
```
#### 安装教程

1.  mv ~/.config/nvim ~/.config/nvim.backup
2.  mv ~/.local/share/nvim ~/.local/share/nvim.backup
3.  cp -r lazynvim ~/.config/
4.  mv ~/.config/lazynvim ~/.config/nvim

#### 使用说明

列举几个经常用的快捷键
键位|说明
---|---
\<leader>| 空格

模式|快捷键|说明|插件
---|:---:|---|---
normal | S | 保存当前文件 |
normal | Q | 退出当前文件 |
insert | jj / jk | 退出insert模式 |
normal | \<learder> sv | 左右分屏|
normal | \<learder> sp | 上下分屏|
normal | ctrl j / k | 快速上下文跳转 |
normal | \<learder> nh | 取消高亮|
normal | - - | 折叠代码 |
normal | tt | 打开一个10行大小的终端 |
normal | > / < | 代码缩进 |
normal | H / L | 切换buffer |
normal | \<learder> fn | 新建文件|
normal | \<learder> fo | 打开历史打开的文件|telescope.nvim
normal | \<learder> ff | 打开当前路经下文件|telescope.nvim
normal | \<learder> fw | 搜索当前目录字符|telescope.nvim
normal | \<learder> fb | 列出当前缓冲区|telescope.nvim
normal | T | 打开文件管理器|neo-tree.nvim
normal | ctrl f | 打开浮动ranger | 依赖 ranger
normal | ctrl t | 打开浮动终端 |
normal | ctrl u | 打开撤销历史 | undotree
normal | vv | 快速选中内容 | vim-expand-region
normal | ff | 高亮光标内容 | vim-interestingwords
normal | gcc | 快速备注光标所在行 | Comment.nvim
visual | gc | 快速注释当前段 |
visual | = | 快速格式化当前段 | coc
normal | F5 | 运行文件 | vim-floaterm
normal | F8 | 打开lazy | lazy

