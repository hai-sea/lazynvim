require('config.lazy')          -- lazy 插件管理
require('config.keymaps')       -- 快捷键
require('config.autocmd')       -- 自动运行
require('config.run')           -- run配置
require('config.options')       -- 基础配置
require("config.neovide")       -- neovide 配置
